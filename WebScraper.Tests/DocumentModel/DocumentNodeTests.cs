﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebScraper.DocumentModel.Models;

namespace WebScraper.Tests.DocumentModel
{
    [TestClass]
    public class DocumentNodeTests
    {
        #region IsRoot

        [TestMethod]
        public void IsRoot_True()
        {
            // Arrange
            IDocumentNode sut = new DocumentNode
            {
                Tag = new Tag("<div>")
            };

            // Act
            // Assert
            Assert.IsTrue(sut.IsRoot);
        }

        [TestMethod]
        public void IsRoot_False()
        {
            // Arrange
            IDocumentNode sut = new DocumentNode
            {
                Tag = new Tag("<div>"),
                Parent = new DocumentNode
                {
                    Tag = new Tag("<div>")
                }
            };

            // Act
            // Assert
            Assert.IsFalse(sut.IsRoot);
        }

        #endregion

        #region IsRoot

        [TestMethod]
        public void HasChildren_True()
        {
            // Arrange
            IDocumentNode sut = new DocumentNode
            {
                Tag = new Tag("<div>"),
                Children = new()
                {
                    new DocumentNode()
                }
            };

            // Act
            // Assert
            Assert.IsTrue(sut.HasChildren);
        }

        [TestMethod]
        public void HasChildren_False()
        {
            // Arrange
            IDocumentNode sut = new DocumentNode
            {
                Tag = new Tag("<div>"),
                Parent = new DocumentNode
                {
                    Tag = new Tag("<div>")
                }
            };

            // Act
            // Assert
            Assert.IsFalse(sut.HasChildren);
        }

        #endregion
    }
}
