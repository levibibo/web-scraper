using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebScraper.DocumentModel;
using WebScraper.DocumentModel.Models;

namespace WebScraper.Tests.DocumentModel
{
    [TestClass]
    public class DocumentTests
    {
        #region SearchTermCount

        [TestMethod]
        public void SearchTermCount_Success_StringFormatMatch()
        {
            // Arrange
            IDocument sut = new Document
            {
                RawText = @"
                    <!doctype html>
                    <html>
                        <head>
                            <title>search results</title>
                        </head>
                        <body>
                            <div>search term</div>
                        </body>
                    </html>
                "
            };
            string searchTerm = "search term";

            // Act
            int response = sut.SearchTermCount(searchTerm);

            // Assert
            Assert.AreEqual(1, response);
        }

        [TestMethod]
        public void SearchTermCount_Success_StringFormatMismatch()
        {
            // Arrange
            IDocument sut = new Document
            {
                RawText = @"
                    <!doctype html>
                    <html>
                        <head>
                            <title>search results</title>
                        </head>
                        <body>
                            <div>sEaRcH TeRm</div>
                        </body>
                    </html>
                "
            };
            string searchTerm = "search term";

            // Act
            int response = sut.SearchTermCount(searchTerm);

            // Assert
            Assert.AreEqual(1, response);
        }

        #endregion
    }
}
