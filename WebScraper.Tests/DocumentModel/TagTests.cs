﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using WebScraper.DocumentModel.Models;

namespace WebScraper.Tests.DocumentModel
{
    [TestClass]
    public class TagTests
    {
        #region GetTagType

        [TestMethod]
        public void GetTagType_HasTagType()
        {
            // Arrange
            ITag sut = new Tag("<div id=\"test-tag-id\" class=\"test-tag-class\">");
            string expected = "div";

            // Act
            // Assert
            Assert.AreEqual(expected, sut.TagType);
        }

        [TestMethod]
        public void GetTagType_IsText()
        {
            // Arrange
            ITag sut = new Tag("<div id=\"test-tag-id\" class=\"test-tag-class\">");
            string expected = "div";

            // Act
            // Assert
            Assert.AreEqual(expected, sut.TagType);
        }

        #endregion

        #region GetID

        [TestMethod]
        public void GetID_HasID()
        {
            // Arrange
            ITag sut = new Tag("<div id=\"test-tag-id\" class=\"test-tag-class\">");
            string expected = "test-tag-id";

            // Act
            // Assert
            Assert.AreEqual(expected, sut.ID);
        }

        [TestMethod]
        public void GetID_HasNoID()
        {
            // Arrange
            ITag sut = new Tag("<div class=\"test-tag-class\">");

            // Act
            // Assert
            Assert.IsNull(sut.ID);
        }

        #endregion

        #region GetClasses

        [TestMethod]
        public void GetClasses_HasClass()
        {
            // Arrange
            ITag sut = new Tag("<div id=\"test-tag-id\" class=\"test-tag-class\">");
            string[] expected = new string[] { "test-tag-class" };

            // Act
            // Assert
            Assert.AreEqual(expected.Length, sut.Classes.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                string expectedClass = expected.ElementAt(i);
                string returnedClass = sut.Classes.ElementAt(i);
                Assert.AreEqual(expectedClass, returnedClass);
            }
        }

        [TestMethod]
        public void GetClasses_HasNoClasses()
        {
            // Arrange
            ITag sut = new Tag("<div id=\"test-tag-id\">");

            // Act
            // Assert
            Assert.IsNull(sut.Classes);
        }

        #endregion
    }
}
