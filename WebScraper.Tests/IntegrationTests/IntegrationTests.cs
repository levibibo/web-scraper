﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using WebScraper.DocumentModel;
using WebScraper.Tools.GoogleAPI;
using WebScraper.Tools.ResponseParser;

namespace WebScraper.Tests.IntegrationTests
{
    [TestClass]
    public class IntegrationTests
    {
        [TestMethod]
        public async Task GetDataIntegrationTest()
        {
            // Arrange
            ISearchAPI api = SearchApiFactory.CreateGoogleAPI();
            IResponseParser parser = ResponseParserFactory.CreateParser();
            string searchTerm = "infotrack";
            int results = 100;

            // Act
            parser.LoadResponse(await api.RunQueryAsync(searchTerm, results));
            IDocument sut = parser.ParseResponse();

            // Assert
            Assert.IsNotNull(sut);
            Assert.IsNotNull(sut.Entrypoint);
            Assert.IsTrue(sut.SearchTermCount(searchTerm) > 0);
        }
    }
}
