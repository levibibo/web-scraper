﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace WebScraper.Tools.GoogleAPI
{
    public class GoogleAPI : ISearchAPI
    {
        public const string BASE_ADDRESS = "https://www.google.com/";
        private readonly string _searchTerm;

        public string SearchEngineName => "Google";
        private static HttpClient Client
        {
            get
            {
                HttpClient client = new();
                client.BaseAddress = new Uri(BASE_ADDRESS);
                return client;
            }
        }

        public GoogleAPI(string searchTerm) => _searchTerm = searchTerm;

        public async Task<string> RunQueryAsync(string searchTerm, int results = 0)
        {
            using HttpClient client = Client;
            searchTerm = string.IsNullOrWhiteSpace(searchTerm) ? _searchTerm : searchTerm;
            HttpRequestMessage request = new(HttpMethod.Get, $"/search?q={searchTerm}&num={results}");
            HttpResponseMessage response = await client.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Error getting response from Google: ReasonPhrase {response.ReasonPhrase}");
            }
            if (response.Content == null)
            {
                throw new Exception("Error getting response from Google: null content");
            }
            string responseString = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrWhiteSpace(responseString))
            {
                throw new Exception("Error getting response from Google: empty content");
            }
            return responseString;
        }
    }
}
