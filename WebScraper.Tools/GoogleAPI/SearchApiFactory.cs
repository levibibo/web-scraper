﻿namespace WebScraper.Tools.GoogleAPI
{
    public static class SearchApiFactory
    {
        public const string SEARCH_TERM = "efiling integration";
        public static ISearchAPI CreateGoogleAPI(string searchTerm = null) => new GoogleAPI(searchTerm ?? SEARCH_TERM);
    }
}
