﻿using System.Threading.Tasks;

namespace WebScraper.Tools.GoogleAPI
{
    public interface ISearchAPI
    {
        string SearchEngineName { get; }
        Task<string> RunQueryAsync(string searchTerm, int results = 0);
    }
}