﻿using WebScraper.DocumentModel;

namespace WebScraper.Tools.Logger
{
    public interface ILogger
    {
        void PrintRawSearch(int count, string searchTarget);
        void PrintLinkTagSearch(int count, string searchTarget);
        void TrackLoadProgress(float percentComplete);
        void TrackFinalLoadProgress();
        void ConfirmResult();
    }
}