﻿namespace WebScraper.Tools.Logger
{
    public static class LoggerFactory
    {
        public static ILogger CreateLogger() => new ConsoleLogger();
    }
}
