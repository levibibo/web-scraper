﻿using System;

namespace WebScraper.Tools.Logger
{
    public class ConsoleLogger : ILogger
    {
        public void PrintRawSearch(int count, string searchTerm)
        {
            Console.WriteLine($"Found {count} instances of search term '{searchTerm}' in the document.");
        }

        public void PrintLinkTagSearch(int count, string searchTerm)
        {
            Console.WriteLine($"Found {count} instances of search term '{searchTerm}' within link tags in the document.");
        }

        public void TrackLoadProgress(float percentComplete)
        {
            string percentDisplay = percentComplete.ToString("0");
            Console.Write($"\rProgress: {percentDisplay}%");
        }

        public void TrackFinalLoadProgress()
        {
            Console.WriteLine($"\rProgress: Complete");
        }

        public void ConfirmResult()
        {
            Console.ReadLine();
        }
    }
}
