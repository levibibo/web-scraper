﻿using WebScraper.DocumentModel;

namespace WebScraper.Tools.ResponseParser
{
    public interface IResponseParser
    {
        void LoadResponse(string response);
        IDocument ParseResponse();
    }
}