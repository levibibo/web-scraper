﻿using WebScraper.DocumentModel.Models;

namespace WebScraper.Tools.ResponseParser
{
    public class ParserData
    {
        public string CurrentText = string.Empty;
        public IDocumentNode CurrentNode;
        public bool ReadingCloseTag = false;
        public bool IgnoringTag = false;

        public bool IsReadingText => !string.IsNullOrWhiteSpace(CurrentText) && !CurrentText.Contains('<');
        public bool CurrentTextIsEmpty => string.IsNullOrEmpty(CurrentText);
        public bool CurrentTextIsEmptyOrWhiteSpace => string.IsNullOrWhiteSpace(CurrentText);
    }
}
