﻿using System;
using WebScraper.DocumentModel;
using WebScraper.DocumentModel.Models;

namespace WebScraper.Tools.ResponseParser
{
    public class ResponseParser : IResponseParser
    {
        private IDocument _document;

        public void LoadResponse(string response)
        {
            _document = DocumentFactory.CreateDocument();
            _document.RawText = response;
        }

        public IDocument ParseResponse()
        {
            ParserData parserData = new();
            parserData.CurrentNode = DocumentFactory.CreateNode();
            _document.Entrypoint = parserData.CurrentNode;
            if (_document.RawText.StartsWith("<!doctype html>", StringComparison.OrdinalIgnoreCase))
            {
                _document.DocType = "html";
                _document.RawText = _document.RawText[15..];
            }
            for (int i = 0; i < _document.RawText.Length; i++)
            {
                char next = _document.RawText[i];

                if (parserData.CurrentText == "<")
                {
                    ParseOpenBracketFirstChar(next, ref parserData);
                }
                else if (next == '<' && parserData.IsReadingText)
                {
                    ParseEndText(next, ref parserData);
                }
                else if (next == '<' && parserData.CurrentTextIsEmpty)
                {
                    parserData.CurrentText += next;
                }
                else if (next == '>' && parserData.CurrentText.Contains('<'))
                {
                    ParseCloseBracket(next, ref parserData);
                }
                else if (!parserData.IgnoringTag || parserData.ReadingCloseTag)
                {
                    if (!string.IsNullOrWhiteSpace(next.ToString()) || !parserData.CurrentTextIsEmptyOrWhiteSpace)
                    {
                        parserData.CurrentText += next;
                    }
                }
            }
            return _document;
        }

        private static void ParseOpenBracketFirstChar(char next, ref ParserData data)
        {
            // is closing tag
            if (next == '/')
            {
                data.ReadingCloseTag = true;
                data.CurrentText += next;
            }
            // is new open tag, create new node - COULD BE JS!
            else if (data.CurrentNode.Tag != null && !data.IgnoringTag)
            {
                IDocumentNode newNode = DocumentFactory.CreateNode();
                newNode.Parent = data.CurrentNode;
                newNode.Depth = newNode.Parent.Depth + 1;
                data.CurrentNode.Children.Add(newNode);
                data.CurrentNode = newNode;
                data.CurrentText += next;
            }
            else if (data.IgnoringTag)
            {
                data.CurrentText = string.Empty;
            }
            else
            {
                data.CurrentText += next;
            }
        }

        private static void ParseCloseBracket(char next, ref ParserData data)
        {
            if (data.ReadingCloseTag)
            {
                // Check if closing tag is the end of an ignored node
                if (!data.IgnoringTag)
                {
                    data.CurrentNode = data.CurrentNode.Parent;
                    data.ReadingCloseTag = false;
                }
                // Make sure closing tag is the node that is currently being ignored
                else if (data.CurrentText.ToLower().Contains($"</{data.CurrentNode.Tag.TagType}"))
                {
                    data.CurrentNode = data.CurrentNode.Parent;
                    data.ReadingCloseTag = false;
                    data.IgnoringTag = false;
                }
                data.CurrentText = string.Empty;
            }
            else
            {
                data.CurrentText += next;
                data.CurrentNode.Tag = DocumentFactory.CreateTag(data.CurrentText);
                // Check if tag is self-closing
                if (data.CurrentText.EndsWith("\\>") || data.CurrentNode.Tag.IsVoidElement)
                {
                    data.CurrentNode = data.CurrentNode.Parent;
                }
                // Check if non-self-closing tag is in ignore list
                else if (data.CurrentNode.Tag.IsIgnoredElement)
                {
                    data.IgnoringTag = true;
                }
                data.CurrentText = string.Empty;
            }
        }

        private static void ParseEndText(char next, ref ParserData data)
        {
            IDocumentNode newNode = DocumentFactory.CreateNode();
            newNode.Tag = DocumentFactory.CreateTag(data.CurrentText);
            newNode.Parent = data.CurrentNode;
            newNode.Depth = newNode.Parent.Depth + 1;
            data.CurrentNode.Children.Add(newNode);
            data.CurrentText = next.ToString();
        }        
    }
}
