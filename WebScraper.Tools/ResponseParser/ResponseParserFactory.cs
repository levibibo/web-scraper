﻿namespace WebScraper.Tools.ResponseParser
{
    public static class ResponseParserFactory
    {
        public static IResponseParser CreateParser()
        {
            return new ResponseParser();
        }
    }
}
