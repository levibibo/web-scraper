﻿namespace WebScraper.Tools.UserPrompt
{
    public interface IUserPrompt
    {
        int PromptUserForNumberOfResults();
        string PromptUserForSearchTarget();
        string PromptUserForSearchTerm(string searchEngineName = "Google");
    }
}