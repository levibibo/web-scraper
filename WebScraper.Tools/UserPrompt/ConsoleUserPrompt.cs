﻿using System;
using WebScraper.Tools.GoogleAPI;

namespace WebScraper.Tools.UserPrompt
{
    public class ConsoleUserPrompt : IUserPrompt
    {
        public const int DEFAULT_RESULTS = 100;
        public const string DEFAULT_SEARCH_TARGET = "infotrack";

        public string PromptUserForSearchTerm(string searchEngineName = "Google")
        {
            Console.WriteLine($"Searching {searchEngineName}");
            Console.Write($"Search term (default: '{SearchApiFactory.SEARCH_TERM}')? ");
            string search = Console.ReadLine();
            return !string.IsNullOrWhiteSpace(search) ? search : SearchApiFactory.SEARCH_TERM;
        }

        public int PromptUserForNumberOfResults()
        {
            Console.Write("Search how many results (default: 100)? ");
            string count = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(count))
            {
                return DEFAULT_RESULTS;
            }
            bool isNumber = int.TryParse(count, out int pages);
            if (!isNumber)
            {
                Console.WriteLine("Please enter a number.");
                return PromptUserForNumberOfResults();
            }
            return pages;
        }

        public string PromptUserForSearchTarget()
        {
            Console.Write($"Search for what term on each page (default: '{DEFAULT_SEARCH_TARGET}')? ");
            string searchTerm = Console.ReadLine();
            return !string.IsNullOrWhiteSpace(searchTerm) ? searchTerm : DEFAULT_SEARCH_TARGET;
        }
    }
}
