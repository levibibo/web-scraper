﻿namespace WebScraper.Tools.UserPrompt
{
    public static class UserPromptFactory
    {
        public static IUserPrompt CreateConsoleUserPrompt() => new ConsoleUserPrompt();
    }
}
