﻿namespace WebScraper.Results
{
    public static class ResultsFactory
    {
        public static IResults CreateResultsState() => new Results();
    }
}
