﻿using System.Collections.Generic;
using WebScraper.DocumentModel;

namespace WebScraper.Results
{
    public class Results : IResults
    {
        public List<IDocument> Documents { get; set; } = new();

        public int RawCount { get; set; } = 0;
        public int AnchorCount { get; set; } = 0;
    }
}
