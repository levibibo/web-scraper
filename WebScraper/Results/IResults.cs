﻿using System.Collections.Generic;
using WebScraper.DocumentModel;

namespace WebScraper.Results
{
    public interface IResults
    {
        int AnchorCount { get; set; }
        List<IDocument> Documents { get; set; }
        int RawCount { get; set; }
    }
}