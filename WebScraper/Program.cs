﻿using System.Threading.Tasks;
using WebScraper.DocumentModel;
using WebScraper.Results;
using WebScraper.Tools.GoogleAPI;
using WebScraper.Tools.Logger;
using WebScraper.Tools.ResponseParser;
using WebScraper.Tools.UserPrompt;

namespace WebScraper
{
    class Program
    {
        static async Task Main(string[] args)
        {
            #region setup

            ISearchAPI searchAPI = SearchApiFactory.CreateGoogleAPI();
            IResponseParser parser = ResponseParserFactory.CreateParser();
            IUserPrompt prompt = UserPromptFactory.CreateConsoleUserPrompt();
            IResults results = ResultsFactory.CreateResultsState();
            ILogger logger = LoggerFactory.CreateLogger();

            #endregion

            #region UserInput

            string searchTerm = prompt.PromptUserForSearchTerm(searchAPI.SearchEngineName);
            int numberOfResults = prompt.PromptUserForNumberOfResults();
            string searchTarget = prompt.PromptUserForSearchTarget();

            #endregion

            #region RunQuery

            parser.LoadResponse(await searchAPI.RunQueryAsync(searchTerm, results: numberOfResults));
            IDocument document = parser.ParseResponse();

            #endregion

            #region GetResults

            results.Documents.Add(document);
            results.RawCount += document.SearchTermCount(searchTarget);
            results.AnchorCount += document.SearchTermCountByTagType(searchTarget, tagType: "a");
            logger.TrackFinalLoadProgress();
            logger.PrintRawSearch(results.RawCount, searchTarget);
            logger.PrintLinkTagSearch(results.AnchorCount, searchTarget);
            logger.ConfirmResult();

            #endregion
        }
    }
}
