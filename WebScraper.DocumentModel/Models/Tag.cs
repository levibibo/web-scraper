﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WebScraper.DocumentModel.Models
{
    public class Tag : ITag
    {
        private readonly static HashSet<string> VoidElements = new()
        {
            "area",
            "base",
            "basefont",
            "bgsound",
            "br",
            "col",
            "command",
            "embed",
            "frame",
            "hr",
            "image",
            "img",
            "input",
            "isindex",
            "keygen",
            "link",
            "menuitem",
            "meta",
            "nextid",
            "param",
            "source",
            "track",
            "wbr"
        };

        private readonly static HashSet<string> IgnoredElements = new()
        {
            "script",
            "style"
        };

        public string TagType { get; set; }
        public string ID { get; set; }
        public string[] Classes { get; set; }
        public string[] Attributes { get; set; }
        public string Text { get; set; }
        public string RawText { get; set; }

        public Tag(string tag)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                return;
            }
            RawText = tag;
            if (!tag.StartsWith('<'))
            {
                Text = tag;
                return;
            }
            tag = tag.Trim(new char[] { '<', '>' });
            tag = GetTagType(tag);
            tag = GetID(tag);
            tag = GetClasses(tag);

            Attributes = tag?.Trim().Split(' ');
        }

        private string GetTagType(string tag)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                return null;
            }
            Match match = Regex.Match(tag, @"^[a-z\-]*(\s|$)", RegexOptions.IgnoreCase);
            if (!match.Success)
            {
                return null;
            }
            TagType = match.Value.TrimEnd().ToLower();
            return tag[TagType.Length..];
        }

        private string GetID(string tag)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                return null;
            }
            Match match = Regex.Match(tag, @"\sid=('|"")[a-zA-Z\-]*('|"")", RegexOptions.IgnoreCase); // double/single quote issue
            if (!match.Success)
            {
                return tag;
            }
            string fullID = match.Value.Trim();
            ID = fullID.Split("\"").ElementAt(1)?.Trim(); // double/single quote issue
            int start = tag.IndexOf(fullID);
            return $"{tag[..start]}{tag[(start + fullID.Length)..]}";
        }

        private string GetClasses(string tag)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                return null;
            }
            Match match = Regex.Match(tag, @"\sclass=('|"")[a-zA-Z\-\s]*('|"")", RegexOptions.IgnoreCase); // double/single quote issue
            if (!match.Success)
            {
                return tag;
            }
            string fullClass = match.Value.Trim();
            Classes = fullClass.Split("\"").ElementAt(1).Split(' '); // double/single quote issue
            int start = tag.IndexOf(fullClass);
            return $"{tag[..start]}{tag[(start + fullClass.Length)..]}";
        }

        public bool IsVoidElement => VoidElements.Contains(TagType);
        public bool IsIgnoredElement => IgnoredElements.Contains(TagType);
    }
}
