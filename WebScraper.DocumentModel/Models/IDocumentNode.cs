﻿using System.Collections.Generic;

namespace WebScraper.DocumentModel.Models
{
    public interface IDocumentNode
    {
        List<IDocumentNode> Children { get; set; }
        IDocumentNode Parent { get; set; }
        ITag Tag { get; set; }
        int Depth { get; set; }
        bool IsRoot { get; }
        bool HasChildren { get; }
    }
}