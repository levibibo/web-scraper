﻿using System.Collections.Generic;
using System.Linq;

namespace WebScraper.DocumentModel.Models
{
    public class DocumentNode : IDocumentNode
    {
        public ITag Tag { get; set; }
        public IDocumentNode Parent { get; set; }
        public List<IDocumentNode> Children { get; set; } = new List<IDocumentNode>();
        public int Depth { get; set; } = 0;

        public bool IsRoot => Parent == null;
        public bool HasChildren => Children != null && Children.Any();
    }
}
