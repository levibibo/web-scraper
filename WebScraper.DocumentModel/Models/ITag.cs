﻿namespace WebScraper.DocumentModel.Models
{
    public interface ITag
    {
        string[] Attributes { get; set; }
        string[] Classes { get; set; }
        string ID { get; set; }
        string TagType { get; set; }
        string Text { get; set; }
        string RawText { get; set; }
        bool IsVoidElement { get; }
        bool IsIgnoredElement { get; }
    }
}