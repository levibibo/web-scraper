﻿using WebScraper.DocumentModel.Models;

namespace WebScraper.DocumentModel
{
    public interface IDocument
    {
        string RawText { get; set; }
        string DocType { get; set; }
        IDocumentNode Entrypoint { get; set; }

        int SearchTermCount(string searchTerm);
        int SearchTermCountByTagType(string searchTerm, string tagType = null);
    }
}