﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using WebScraper.DocumentModel.Models;

namespace WebScraper.DocumentModel
{
    public class Document : IDocument
    {
        public string RawText { get; set; }
        public string DocType { get; set; }
        public IDocumentNode Entrypoint { get; set; }

        public int SearchTermCount(string searchTerm)
        {
            MatchCollection matches = Regex.Matches(RawText, searchTerm, RegexOptions.IgnoreCase);
            return matches.Count;
        }

        public int SearchTermCountByTagType(string searchTerm, string tagType = null)
        {
            List<IDocumentNode> includedNodes = new();
            CheckNode(Entrypoint, ref includedNodes, searchTerm, tagType);
            return includedNodes.Count;
        }

        public void CheckNode(IDocumentNode node, ref List<IDocumentNode> includedNodes, string searchTerm, string tagType)
        {
            if (!string.IsNullOrWhiteSpace(node.Tag.TagType))
            {
                if ((tagType == null || node.Tag.TagType == tagType) &&
                    node.Tag.RawText.Contains(searchTerm, StringComparison.OrdinalIgnoreCase))
                {
                    includedNodes.Add(node);
                }
            }
            if (node.HasChildren)
            {
                foreach(IDocumentNode n in node.Children)
                {
                    CheckNode(n, ref includedNodes, searchTerm, tagType);
                }
            }
        }
    }
}
