﻿using WebScraper.DocumentModel.Models;

namespace WebScraper.DocumentModel
{
    public static class DocumentFactory
    {
        public static IDocument CreateDocument() => new Document();
        public static IDocumentNode CreateNode() => new DocumentNode();
        public static ITag CreateTag(string tag) => new Tag(tag);
    }
}
